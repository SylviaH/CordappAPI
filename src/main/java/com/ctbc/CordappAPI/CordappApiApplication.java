package com.ctbc.CordappAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
public class CordappApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CordappApiApplication.class, args);
	}
}
