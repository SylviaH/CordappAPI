package com.ctbc.CordappAPI;

import com.google.common.collect.ImmutableMap;
import net.corda.core.contracts.ContractState;
import net.corda.core.identity.Party;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/node")
class NodeController {
    @Autowired
    private NodeRPCConnection nodeRPCConnection;
    private Party myIdentity;

    static private final Logger logger = LoggerFactory.getLogger(NodeController.class);

    @GetMapping("/me")
    public String me(){
        this.myIdentity = nodeRPCConnection.rpcOps.nodeInfo().getLegalIdentities().get(0);
        return myIdentity.toString();
    }

    @GetMapping("/servertime")
    public String servertime(){
        return Instant.now().atZone(ZoneId.of("UTC")).toString();
    }

    @GetMapping("/address")
    public String address(){
        return nodeRPCConnection.rpcOps.nodeInfo().getAddresses().toString();
    }

    @GetMapping("/platformversion")
    public int platformversion(){
        return nodeRPCConnection.rpcOps.nodeInfo().getPlatformVersion();
    }

    @GetMapping("/peers")
    public Map<String, List<String>> peers(){
        return ImmutableMap.of("peers",nodeRPCConnection.rpcOps.networkMapSnapshot()
                .stream()
                .filter(nodeInfo -> nodeInfo.getLegalIdentities().get(0) != myIdentity)
                .map(it -> it.getLegalIdentities().get(0).getName().getOrganisation())
                .collect(Collectors.toList()));
    }

    @GetMapping("/notaries")
    public String notaries(){
        return nodeRPCConnection.rpcOps.notaryIdentities().toString();
    }

    @GetMapping("/flow")
    public String flow(){
        return nodeRPCConnection.rpcOps.registeredFlows().toString();
    }

    @GetMapping("/states")
    public String states(){
        return nodeRPCConnection.rpcOps.vaultQuery(ContractState.class).getStates().toString();
    }
}
